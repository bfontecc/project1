<?php
/**
 * index.php
 * default controller/dispatcher for project1
 */
session_start();

//load model
include_once("../models/db_model.php");

//upon successful login, login controller will set $_SESSION['auth'] to 'true'
//upon logout, session data should not be set
if (isset($_SESSION['auth']) && $_SESSION['auth'] === true) {
	$auth = true;
} else {
	$auth = false;
}

//set variables $title, $controller
extract(parse_get());
//header including title, nav-bar, title-well, bootstrap
//controller will set $content after setting all the vars needed by that view
include_once("../controllers/{$controller}.php");
//content_loader will load $content into div
include_once("../controllers/content_loader.php");
//footer
include_once("../views/templates/footer.php");

/**
 * parse_get()
 *
 * reads the GET data and dispatches appropriate controller
 * returns array with keys 'content', 'title',
 *
 * @return array dispatch_data
 */
function parse_get() {
	if (!isset($_GET['q'])) {
		$_GET['q'] = 'home';
	}
	switch ($_GET['q']) {
		default:
			$title = "Home";
			$controller = "home";
			break;
		case 'home':
			$title = "Home";
			$controller = "home";
			break;
		case 'sell':
			$title = 'Sell Shares';
			$controller = 'sell';
			break;
		case 'login':
			$title = "Login";
			$controller = "login";
			break;
		case 'logout':
			$title = "Logout";
			$controller = "logout";
			break;
		case 'register':
			$title = "Register";
			$controller = "register";
			break;
		case 'portfolio':
			$title = "View and Edit Portfolio";
			$controller = "portfolio";
			break;
		case 'buy':
			$title = "Buy Stock";
			$controller = "buy";
			break;
		case 'quote':
			$title = "Stock Quote";
			$controller = 'quote';
			break;
		case 'login_successful':
			$title = "Success!";
			$controller = "login_successful"; 
	}
	return array 
			(	
				'title' => $title,
				'controller' => $controller
			);
}
?>