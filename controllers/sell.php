<?
/**
 * sell controller
 *
 * simply passes portfolio holding form data to model, calls on sell view
 * to inform user of result, offer links.
 */

$msg = "Unknown Status";
$c_id = false;
$u_id = false;


if (!isset($_POST['company_id'])):
	$msg = "You must sell a stock by clicking Portfolio and then Sell All";
else:
	$c_id = $_POST['company_id'];
	$c = true;
endif;

if (!isset($_SESSION['user_id'])):
	$msg = "You must log in to make a transaction";
else:
	$u_id = $_SESSION['user_id'];
	$u = true;
endif;

if ($c && $u):
	$sell_result = sell($u_id, $c_id);
	if(!$sell_result === false):
		$sell_result = sprintf("%01.2f", $sell_result);
		$msg = "Success! Your stocks have been sold for $" . $sell_result;
	else:
		$msg = "There was a problem with your transaction. It has been cancelled.";
	endif;
endif;

include_once("../views/templates/header.php");
$content = 'msg';
?>