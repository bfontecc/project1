<?php
/**
 * content_loader.php
 *
 * Loads the view for a particular type of content.
 * Places content into a bootstrap "well" and loads in parameters.
 * Should be required by default controller/dispatcher (/html/index.php).
 */
 ?>
<div class="well">
<?php 
if (!isset($content)) {
	$content = 'home';
}
include_once("../views/{$content}.php"); 
?>
</div>