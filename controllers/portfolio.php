<?php
/**
 * portfolio controller
 *
 * offers user a login view if not authorized
 * displays portfolio as forms allowing sell function if holdings available.
 */

if (!isset($_SESSION['user_id'])):
	$content = 'login';	//&redir=pf careful xss- cut length, use htmlchars
else:
	$user_id = $_SESSION['user_id'];
	$portfolio = get_portfolio_array($user_id);	//ask model for portfolio
	//ask for model for quotes, feed holdings values to view
	$i = 0;	//foreach counter
	$total = 0;
	foreach ($portfolio as $holding):
		$q = fetch_quote($holding['ticker']);	//get quote array
		$lt = $q['last_trade'];					//take last trade from it
		//store it in $portfolio
		$portfolio[$i]['price'] = $lt;
		$total += $lt * $holding['num_shares'];
		$i++;
	endforeach;
	$cash = get_cash($user_id);
	$total += $cash;
	if ($cash === false)
		$cash = "There was a problem checking your balance";
	$content = 'portfolio';
endif;

include_once("../views/templates/header.php");

?>