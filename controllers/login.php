<?php
/**
 * login controller
 *
 * Checks POST data for login entry. If none, offers form via login view
 * The login view's form posts back to here. Once POST data is available,
 * this login controller loads the home view, having set the session
 */

//if no post data
if (!isset($_POST['email']) || !isset($_POST['pass'])) {
	$content = 'login';
} else {
	//talk to model and see if it's right
	$user_id = user_login($_POST['email'], $_POST['pass']);
	if ($user_id === false) {
		echo "<script>alert('Login Failed');</script>";
		//considered being more verbose here
		//figured hackers are more interested than any one else
		$content = 'login';
		//Here the browser will ask whether to resubmit data.
		//I find those alerts annoying, but it's a valid question here.
		//I considered repopulating the field with the email address if it's in the db,
		//but security concerns arose again.
	} else { //$user_id !== false
		//debugging code
		echo "user_id: " . $user_id;
		set_session_is_auth($user_id);
		$auth = true;
		$content = 'login_successful';
	}
}
include_once("../views/templates/header.php");

?>