<?php
/**
 * logout controller
 * destroys session data
 * sets cookie to negative expiration
 * 
 * sets $content to logout view
 */

unset($_SESSION['user_id']);
if (isset($_COOKIE['session_name()'])) {
	setcookie(session_name(), '', time() - 1000 * 1000, '/');
}
session_destroy();
$auth = false;
$content = 'logout';
include_once("../views/templates/header.php");
?>
