<?php
/**
 * register controller
 *
 * Checks POST data, and registers user if valid credentials passed.
 * generates error message upon invalid attempts.
 *
 * Offers registration form if need be, or else Registration success page
 * where applicable.
 */
 
include_once("../views/templates/header.php");
 
$msg = "Enter your email address and a password between 6 and 30 characters,
	with a combination of letters and numbers, but no symbols.";
$email = get_email();
$password = get_password();

//these will be false if valid, an error message if malformed
$email_mal = email_malformed($email);
$pass_mal = password_malformed($password);

if ($email && $password):
	if (!$email_mal && !$pass_mal):
		$model_result = register_user($email, $password);
		if ($model_result['register_status']):
			$content = 'registration_successful';
		else:
			$msg .= $model_result['register_error'];
			$content = 'register';
		endif;
	else:
		$content = 'register';
		if ($email_mal)
			$msg .= "<br><br>" . $email_mal;
		if ($pass_mal)
			$msg .= "<br><br>" . $pass_mal;
	endif;
else:
	$content = 'register';
endif;

/**
 * get_password()
 * retrieves password from post data.
 * returns false if not set.
 * @return [false | string $password]
 */
function get_password() {
	if (!isset($_POST['pass'])):
		return false;
	else:
		return $_POST['pass'];
	endif;
}

/**
 * get_email()
 * retrieves email from post data.
 * returns false if not set.
 * @return [false | string $email]
 */
function get_email() {
	if (!isset($_POST['email'])):
		return false;
	else:
		return $_POST['email'];
	endif;
}

/**
 * email_malformed()
 * calls functions check_length(), check_email_syntax(), escape_check()
 * @param string $email
 * @return [false | string $error]
 */
function email_malformed($email) {
	$error = false;
	if (!check_length($email, 6, 50)):
		return "Email address must be between 6 and 50 characters in length.";
	elseif (!check_email_syntax($email)):
		return "Email address must be in the form name@domain";
	elseif (!escape_check($email)):
		return "Invalid Email address.";
	endif;
	return $error;
}
 
/**
 * password_malformed()
 * calls functions check_length(), is_homogenous(), escape_check()
 * @param string $password
 * @return [false | string $error]
 */
function password_malformed($password) {
	$error = false;
	if (!check_length($password, 6, 30)):
		return "Password must be between 6 and 50 characters in length.";
	elseif (is_homogenous($password)):
		return "Password must contain letters AND numbers.";
	elseif (!escape_check($password)):
		return "Password may not contain symbols other than letters and numbers.";
	endif;
	return $error;
}

/**
 * check_length()
 * length must be within or equal to min, max
 * @param numeric $min
 * @param numeric $max
 * @param string $str
 * @return bool $correct_len
 */
function check_length($min, $max, $str) {
	if (strlen($str) < $min || strlen($str) > $max):
		return false;
	else:
		return true;
	endif;
}

/**
 * check_email_syntax()
 * @param string $email
 * @return bool $correct_syntax
 */
function check_email_syntax($email) {
	return filter_var($email, FILTER_VALIDATE_EMAIL);
}

/**
 * escape_check()
 * applies htmlspecialchars to a string, and determines whether it has changed
 * @param string $str
 * @return
 */
function escape_check($str) {
	if ($str != htmlspecialchars($str)):
		return false;
	else:
		return true;
	endif;
}

/**
 * is_homogenous()
 * @param string $password
 * @return bool $homogenous
 */
function is_homogenous($password) {
	$num_pattern = "/[0-9]/";
	$alpha_pattern = "/[a-zA-Z]/";
	if (preg_match($num_pattern, $password)
		&& preg_match($alpha_pattern, $password)):
		return false;
	else:
		return true;
	endif;
}
