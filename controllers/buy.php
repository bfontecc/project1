<?php
/**
 * buy controller
 *
 * looks for post data from buy view
 *		executes buy if posted, then displays msg view
 * offers buy view (purchase form) if no purchase posted.
 */
$msg = "Unknown Status";
$ticker = false;
$user_id = false;

$user_id = get_user_id();
$check_post = check_post();
if($user_id):
	if (check_post() === true):
		$msg = buy($user_id, $_POST['num_shares'],  $_POST['ticker']);
		$content = 'msg';
	else:
		$msg = $check_post;
		$cash = get_cash($user_id);
		$content = 'buy';
	endif;
else:
	$content = 'login';
endif;

/**
 * get_user_id()
 * @return [false | numeric $user_id]
 */
function get_user_id () {
	if (!isset($_SESSION['user_id'])):
		return false;
	else:
		return $_SESSION['user_id'];
	endif;
}

/**
 * check_post()
 * 
 * returns str $msg if post data not conducive to buying
 * returns true if ready to do transaction
 */
function check_post() {
	$num_shares = false;
	$ticker = false;
	if (isset($_POST['ticker']))
		$ticker = $_POST['ticker'];
	if (isset($_POST['num_shares']))
		$num_shares = $_POST['num_shares'];
	if ($num_shares && $ticker)
		return true;
	if ($num_shares && !$ticker)
		return "You must enter a stock ticker";
	if (!$num_shares && $ticker)
		return "You must enter a number of shares.";
	if (!$num_shares && !$ticker)
		return "Purchase shares here";
}
include_once("../views/templates/header.php");
?>