<?php
/**
 * quote controller
 * retrieves quote from model
 * feeds quote results to quote view
 */
 
include_once("../views/templates/header.php");

//look up stock quote
if (isset($_GET['ticker'])) {
	$yhoo_results = fetch_quote($_GET['ticker']);
	//should extract $name, $last_trade, $ticker
	if($yhoo_results)
		extract($yhoo_results);
}

$content = 'quote';