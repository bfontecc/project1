<?php
/**
 * login_successful view
 * simply notifies the user that they are logged in,
 * offers links to homepage and logout.
 */
?>
<!-- login_successful view -->
<h2>SUCCESS! You are now logged in.</h2>
<br>
<p>
	Now that you are logged in, you may access your <a href=index.php?q=portfolio>portfolio</a>, 
	<a href="index.php?q=buy"> buy</a> stock, or sell stock from your portfolio by clicking on
	portfolio in the navigation bar.
</p>
<a class="btn btn-primary btn-large" href="index.php?q=home">
Home
</a>
<a class="btn" href="index.php?q=logout">
Logout
</a>
<!-- /login_successful view -->