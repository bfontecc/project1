<?php
/**
 * quote view
 * 
 * presents form for fetching yahoo finance quote
 * if given results from controller, displays them
 */
if (!isset($ticker))
	$ticker = "";
if (!isset($last_trade))
	$last_trade = "find out now!";
if (!isset($name))
	$name = "Enter above.";
?>
<!-- quote view --> 
<h4>Enter Ticker</h4>
<form method='get' action='index.php'>
	<input type='hidden' name='q' value='quote'>
	<input type='text' class='input-small' name='ticker' value=<?php echo "\"" . $ticker . "\""; ?>>
	<br>
	<button type='submit' class='btn btn-primary'>Get Quote!</button>
</form>
<table class="table-bordered table-striped">
	<tr>
		<td><b>Company:</b></td>
		<td><?php echo $name; ?></td>
	</tr>
	<tr>
		<td><b>Last Price:</b></td>
		<td><?php echo $last_trade; ?></td>
	</tr>
</table>
<!-- /quote view -->