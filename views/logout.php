<?php
/**
 * logout view
 *
 * a simple view which offers links to log in and go home
 */
 
?>
<h3>You have been logged out!</h3>
<a class="btn btn-primary btn-large" href="index.php?q=home">
Home
</a>
<a class="btn" href="index.php?q=login">
Login
</a>