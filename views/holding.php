<?php
/**
 * holding view
 * creates a form which simultaneously displays a stock holding, and offers a sell feature.
 * intended to be displayed in conjuction with portfolio view.
 */

/**
 * disp_hold()
 * displays one inline holding form.
 *
 * @param $company_id
 * @param $ticker
 * @param $num_shares
 * @param $company_name
 * @param $price
 */
function disp_hold($company_id, $ticker, $num_shares, $company_name, $price) {
	$sp = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
	$value = $price * $num_shares;
	$value = sprintf("%01.2f", $value);
	echo "<form class='well form-inline' method='post' action='index.php?q=sell'>";
	echo "<input type='hidden' name='company_id' value='" . $company_id. "'>";
	echo "<label><b>Company: </b>" . $company_name . $sp. "</label>";
	echo "<label><b>Ticker: </b>" . $ticker . $sp . "</label>";
	echo "<label><b>Shares: </b>" . $num_shares . $sp. "</label>";
	echo "<label><b>Price</b> (delayed): $" . $price . $sp. "</label>";
	echo "<br><label><b>Estimated Value</b> (delayed): $" . $value . "</label>";
	echo "<button type='submit' class='btn pull-right btn-warning'>Sell All</button>";
	echo "</form>";
}