<?php
/*
 * header.php
 * by Bret Fontecchio
 *
 * Constructs a header dynamically.
 * expects $title var available upon inclusion.
 */

if (!isset($title)) {
    $title = "";
}
$title_prepend = "C$-75 Finance -- ";
$title = htmlspecialchars($title_prepend.$title);
?>
<!-- header -->
<!DOCTYPE html>
<html>
    <head>
        <title> 
            <?php echo $title ?>
        </title>

    <!--Twitter Bootstrap CSS Library-->
    <!--license: https://github.com/twitter/bootstrap/wiki/License-->
    <link rel="stylesheet" href="bootstrap/docs/assets/css/bootstrap.css">
    
    <style type="text/css">
    body { 
    	padding-top: 55px;
    	background-image: url('/images/background.jpg');  
		background-repeat:repeat; 
	}
	</style>
	<!-- 
	background image source:
		http://webdesignledger.com/freebies/40-beautiful-patterns-and-textures-for-ornate-backgrounds
	-->
	<?php include_once("../views/templates/nav_bar.php"); ?>
	<?php include_once("../views/templates/title_well.php"); ?>
    </head>
    <body>
<!-- /header -->
