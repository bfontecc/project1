<?php
/**
 * registration_successful view
 *
 * creates a form for email address and password
 * passes a message from the controller.
 */
?>
<!-- registration_successful view -->
<h3>Registration Successful</h3>
<a class="btn btn-primary" href="index.php?q=login">
Login
</a>
<!-- /registration_successful view -->