<?php
/**
 * buy view
 *
 * displays a form requesting ticker and number of shares
 * posts it to buy controller
 */
?>
<h3><?php echo $msg; ?></h3>
<form action='index.php?q=buy' method='post'>
	<label>Ticker: </label>
	<input type='text' class='input-small' name='ticker'>
	<br>
	<label>Number of Shares</label>
	<input type='text' class='input-small' name='num_shares' value='0'>
	<br>
	<button type="submit" class="btn btn-success">Buy!</button>
</form>
<?
$cash = sprintf("%01.2f", $cash);
echo "<h3>Cash: \${$cash}</h3>";
?>