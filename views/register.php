<?php
/**
 * register view
 *
 * creates a form for email address and password
 * passes a message from the controller.
 */
echo "<!-- register view -->";
echo "<p>" . $msg . "</p>";
?>

<form method='post' action='index.php?q=register'>
	<label>Email:</label>
	<input type='text' name='email'>
	<br>
	<label>Password:</label>
	<input type='password' name='pass'>
	<br>
	<button type='submit' class='btn btn-primary'>Submit</button>
</form>
<!-- /register view -->