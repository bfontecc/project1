<?php
/**
 * portfolio view
 *
 * displays a user's cash assets.
 * intended to be displayed in conjunction with holdings views.
 * offers buy link.
 */

?>
<!-- portfolio view -->
<p>Welcome to your portfolio. From here, you can view your cash account balance,
see your stock portfolio, and sell your holdings. To buy stocks, visit the
<a href='index.php?q=buy'> Buy</a> page.</p>
<?php
include_once("../views/holding.php");
$count = 0;
foreach ($portfolio as $holding):
	$count++;
	disp_hold(	$holding['company_id'], 
				$holding['ticker'], 
				$holding['num_shares'], 
				$holding['company_name'],
				$holding['price']
				);
endforeach;
echo "<h3>Companies in portfolio: {$count}</h3>";
$cash = sprintf("%01.2f", $cash);
echo "<h3>Cash: \${$cash}</h3>";
$total = sprintf("%01.2f", $total);
echo "<h3>Portfolio Worth: \${$total}</h3>";
?>
<!-- /portfolio view -->