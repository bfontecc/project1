<?php
/**
 * db_model.php
 * @author Bret Fontecchio
 *
 * This is the primary and perhaps only model for project1.
 * The mysql database containing user login credentials and portfolios will
 * be maintained and accessed here.
 *
 * salting and hashing is done during query preparation
 *
 * Maintains databases in Third Normal Form.
 * is based on skeleton provided by 
 * @author Chris Gerber
 * 
 */

//database variables - based on Chris Gerber's source
define('DB_HOST', 'localhost');
define('DB_USER', 'jharvard');
define('DB_PASSWORD', 'crimson');
define('DB_DATABASE', 'jharvard_project1');
define('SALT', '$$$');

/**
 * get_handle()
 * connect to mysql server, select database, and return handle
 * 
 * @return PDO Object $dbh
 */
 function get_handle() {
 	$dbh = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_DATABASE, DB_USER, DB_PASSWORD);
 	return $dbh;
 }
 
/**
 * salt_and_hash()
 * adds salt constant defined above to a string
 * uses SHA1 to hash the salted string
 *
 * @param string $pass
 * @return string $password_hash
 */
function salt_and_hash($pass) {
	$pass .= SALT;
	return hash("SHA1", $pass);
}

/**
 * get_check_query_combo()
 * Prepares a PDO SQL statement for checking whether an email and password combo
 * is in the users table and selects user_id for that row. 
 * Takes password, NOT password hash.
 *
 * @param PDO Object $dbh
 * @param string $email
 * @param string $pass
 *
 * @return PDOStatement Object $check_query
 */
function get_check_query_combo($dbh, $email, $pass) {
	$email = strtolower($email);
	$password_hash = salt_and_hash($pass);
	//prepare and bind query statement
	$check_query = $dbh->prepare("
								SELECT user_id FROM users 
								WHERE LOWER(email) = :email_bind
								AND password_hash = :hash_bind
								");
	$check_query->bindValue(':email_bind', $email, PDO::PARAM_STR);
	$check_query->bindValue(':hash_bind', $password_hash, PDO::PARAM_STR);
	return $check_query;
}

/**
 * get_check_query_email()
 * Prepares a PDO SQL statement for checking whether an email
 * is in the users table and selects user_id for that row. 
 *
 * @param PDO Object $dbh
 * @param string $email
 *
 * @return PDOStatement Object $check_query
 */
function get_check_query_email($dbh, $email) {
	$email = strtolower($email);
	//prepare and bind query statement
	$check_query = $dbh->prepare("
								SELECT user_id FROM users 
								WHERE LOWER(email) = :email_bind
								");
	$check_query->bindValue(':email_bind', $email, PDO::PARAM_STR);
	return $check_query;
}

/**
 * get_insert_user_query()
 * Prepares a PDO SQL statement for inserting a user email and pass into users
 * Takes password, NOT password hash.
 *
 * @param PDO Object $dbh
 * @param string $email
 * @param string $pass
 *
 * @return PDOStatement Object $check_query
 */
function get_insert_user_query($dbh, $email, $pass) {
	$email = strtolower($email);
	$password_hash = salt_and_hash($pass);
	//prepare and bind query statement
	$insert_query = $dbh->prepare("
								INSERT INTO users (email, password_hash, cash)
								VALUES (:email_bind, :hash_bind, 10000);
								");
	$insert_query->bindValue(':email_bind', $email, PDO::PARAM_STR);
	$insert_query->bindValue(':hash_bind', $password_hash, PDO::PARAM_STR);
	return $insert_query;
}

/**
 * check_user_id()
 * Determines whether there is one and only one user_id in an array returned by
 * PDOStatement::fetchAll
 * If so, returns it. If not, returns false.
 *
 * @param array $query_results
 * @return [false | numeric user_id]
 */
function check_user_id($query_results) {
	if (
		!isset($query_results[0]) 
		|| !isset($query_results[0]['user_id'])
		|| isset($query_results[1]['user_id'])
		) {
		return false;
	} else {
		return $query_results[0]['user_id'];
	}
}

/**
 * register_user()
 * Create new row in table users.
 *
 * returns false on failure
 *
 * @param string $email
 * @param string $pass
 * 
 * @return array $status
 * array keys:
 * 'register_error' => string
 * 'register_status' => bool
 */
 function register_user($email, $pass) {
	$dbh = get_handle();
	//prepare PDO statements
	$check_query = get_check_query_email($dbh, $email);
	$insert_query = get_insert_user_query($dbh, $email, $pass);
	$dbh->beginTransaction();
	$check_query->execute();
	$check_result = $check_query->fetchAll();
	if (check_user_id($check_result)):
		$dbh->rollback();
		return array('register_status' => false, 'register_error' => 'That email address is taken.');
	else:
		if($insert_query->execute()):
			$dbh->commit();
			return array('register_status' => true, 'register_error' => 'Success. No error.');
		else:
			$dbh->rollback();
			return array('register_status' => false, 'register_error' => $insert_query->errorCode());
		endif;
	endif;
 }
  

 /**
  * user_login()
  * Check user credentials against database.
  * Return numeric user_id from users table
  *
  * Returns false on any failure to log in.
  *
  * @param string $email
  * @param string $pass
  *
  * @return int $user_id
  */
function user_login($email, $pass) {
	//get database handle PDO Object
	$dbh = get_handle();
	//prepare and bind query statement
	$check_query = get_check_query_combo($dbh, $email, $pass);
	//execute statement and fetch results
	$check_query->execute();
	$query_results = $check_query->fetchAll();
	//check and return user_id
	$user_id = check_user_id($query_results);
	return $user_id;
}

/**
 * set_session_is_auth()
 *
 * sets the user's session 'auth' bool field to true
 * stores the user_id in the session.
 *
 * @param numeric $user_id
 */
function set_session_is_auth($user_id) {
	$_SESSION['auth'] = true;
	$_SESSION['user_id'] = $user_id;
}

/**
 * get_portfolio_array()
 * returns numeric array of holdings arrays
 * holdings arrays contain keys "ticker", "company_name", "num_shares", "company_id"
 *
 * returns false on failure
 *
 * @param $user_id
 * @return array portfolio_array
 */
function get_portfolio_array($user_id) {
	$dbh = get_handle();
    $holdings_query = $dbh->prepare("
								SELECT ticker, company_name, num_shares, companies.company_id
								FROM holdings, companies
								WHERE holdings.user_id=:user_id_bind
								AND holdings.company_id=companies.company_id;
								");
	$holdings_query->bindValue(':user_id_bind', $user_id, PDO::PARAM_INT);
	$holdings_query->execute();
	$holdings = $holdings_query->fetchAll();
	return $holdings;
}

/**
 * fetch_quote()
 * returns associative array representing Yahoo! Finance stock quote
 * array keys "ticker", "last_price", "company_name"
 *
 * returns false on failure
 *
 * @author Chris Gerber
 * This function was in section code essentially as it is here.
 * I tried to rewrite my own and it came out basically the same,
 * as there are not many straightforward ways to do this.
 * I considered differentiating ask (a) and bid (b) prices to include broker fees
 * in transactions, but that felt like overkill because we didn't do that
 * in lecture or section, but used last trade (l1).
 *
 * http://www.gummy-stuff.org/Yahoo-data.htm
 *
 * @param string $ticker
 * @return [false | array $quote_results]
 */
 function fetch_quote($ticker) {
	$quote_results = false;
	$yhoo_query = "http://download.finance.yahoo.com/d/quotes/.csv?s={$ticker}&f=sl1n&e=.csv";
	$handle = fopen($yhoo_query, "r");
	$row = fgetcsv($handle);
	//print_r($row);	//dbg
	//I used empty here because I want to check for an array which exists but contains null values
	if (!empty($row) && isset($row[0]) && isset($row[1]) && isset($row[2])) {
		$quote_results = array  (
								'ticker' => $row[0],
								'last_trade' => $row[1], 
								'name' => $row[2]
								);
	}
	fclose($handle);
	//print_r($quote_results);	//dbg
	return $quote_results;
 }
 
 /**
  * get_ticker()
  * 
  * @param $company_id
  * @return string $ticker
  */
 function get_ticker($company_id) {
 	$dbh = get_handle();
 	$ticker_query = $dbh->prepare("SELECT ticker FROM companies
									WHERE company_id=:company_id_bind");
	$ticker_query->bindValue(':company_id_bind', $company_id, PDO::PARAM_INT);
	$ticker_query->execute();
	$ticker_result = $ticker_query->fetchAll();
	if(isset($ticker_result[0]['ticker'])):
		//echo "ticker: " . $ticker_result[0]['ticker'];	//dbg
		return $ticker_result[0]['ticker'];
	else:
		return false;
	endif;
 }
  /**
  * get_company_id()
  * 
  * @param $ticker
  * @return [false | numeric $company_id]
  */
 function get_company_id($ticker) {
 $dbh = get_handle();
 	$id_query = $dbh->prepare("SELECT company_id FROM companies
									WHERE ticker=:ticker_bind");
	$id_query->bindValue(':ticker_bind', $ticker, PDO::PARAM_STR);
	$id_query->execute();
	$id_result = $id_query->fetchAll();
	if(isset($id_result[0]['company_id'])):
		//echo "id: " . $id_result[0]['company_id'];	//dbg
		return $id_result[0]['company_id'];
	else:
		return false;
	endif;
 }
 
/**
 * insert_company_info()
 * add company name and ticker into companies table
 *
 * @param ticker
 * @return bool success
 */
function insert_company_info($ticker) {
	$ticker = strtoupper($ticker);
	$dbh = get_handle();
	$dbh->beginTransaction();				//begin transaction
	if (get_company_id($ticker) === false):	//if company is not found in db
		$quote = fetch_quote($ticker);		//get quote to find name
		$name = $quote['name'];
		if (!isset($quote['last_trade'])):
			//echo "can't get quote";			//dbg
			return false;
		endif;
		if (!intval($quote['last_trade']) > 0):
			//echo "quoted at zero";			//dbg
			return false;
		endif;
		//echo $quote['last_trade'];			//dbg
		//prepare statement to add company name and ticker
		$insert_query = $dbh->prepare("INSERT INTO companies (ticker, company_name)
										VALUES (:ticker_bind, :company_name_bind);");
		$insert_query->bindValue(':ticker_bind', $ticker, PDO::PARAM_STR);
		$insert_query->bindValue(':company_name_bind', $name, PDO::PARAM_STR);
		$insert_query->execute();
		$dbh->commit();
		return true;
	endif;
}
 
/**
 * get_cash()
 * Returns the amount of cash a user has.
 *
 * @param user_id
 * @return [false | numeric $cash]
 */
function get_cash($user_id) {
    $dbh = get_handle();
    $cash_query = $dbh->prepare("SELECT cash FROM users WHERE user_id=:user_id_bind");
    $cash_query->bindValue(':user_id_bind', $user_id, PDO::PARAM_INT);
    $cash_query->execute();
    $cash = $cash_query->fetchAll();
    if (isset($cash[0]['cash'])):
    	$cash = $cash[0]['cash'];
    	return $cash;
    else:
    	return false;
    endif;
}
 
 /**
  * add_cash()
  * 
  * begin transaction, determine cash in the user's account,
  * add the passed amount to it, update db
  *
  * @param $user_id
  * @param $amount
  * @return bool $success
  */
function add_cash($user_id, $amount) {
	$dbh = get_handle();
 	$update_query = $dbh->prepare("UPDATE users SET cash=:amount_bind
 									WHERE user_id=:user_id_bind;");
 	$update_query->bindValue(':user_id_bind', $user_id, PDO::PARAM_INT);
 	//can't bind amount yet, it's not determined
	$dbh->beginTransaction();					//begin transaction
	$cash = get_cash($user_id);	//determine the cash they have
	if(!isset($cash)):		//if it's not set somethings wrong
		$dbh->rollback();
		return false;
	endif;
	$amount += $cash;		//add up the new figure
	$update_query->bindValue(':amount_bind', $amount, PDO::PARAM_STR);
	if($update_query->execute()):				//attempt update
		$dbh->commit();
		return true;							//success
	else:
		$dbh->rollback();
		return false;							//failure
	endif;
}

/**
 * add_holding()
 *
 * insert into holdings table.
 * does NOT check whether it already exists
 *
 * @param $user_id
 * @param $company_id
 * @param $num_shares
 * @param $dbh
 *
 *	@return bool success
 */
function add_holding($user_id, $company_id, $num_shares, $dbh) {
	//prepare add holding statement
	$insert_holding = $dbh->prepare("INSERT INTO holdings 
									(user_id, company_id, num_shares)
									VALUES
									(:user_bind, :company_id_bind, :num_bind)");
	$insert_holding->bindValue(':user_bind', $user_id, PDO::PARAM_INT);
	$insert_holding->bindValue(':company_id_bind', $company_id, PDO::PARAM_INT);
	$insert_holding->bindValue(':num_bind', $num_shares, PDO::PARAM_INT);
	if ($insert_holding->execute()):
		return true;
	else:
		return false;
	endif;
}
 
/**
 * update_holding()
 *
 * update the number of shares in a holdings entry
 * does NOT check whether that entry exists
 *
 * @param $user_id
 * @param $company_id
 * @param $num_shares
 * @param $dbh
 *
 *	@return bool success
 */
function update_holding($user_id, $company_id, $num_shares, $dbh) {
	//prepare update holding statement
	$update_holding = $dbh->prepare("UPDATE holdings
									SET num_shares=:num_bind
									WHERE company_id=:company_id_bind
									AND user_id=:user_bind;"
									);
	$update_holding->bindValue(':user_bind', $user_id, PDO::PARAM_INT);
	$update_holding->bindValue(':company_id_bind', $company_id, PDO::PARAM_INT);
	$update_holding->bindValue(':num_bind', $num_shares, PDO::PARAM_INT);
	if ($update_holding->execute()):
		return true;
	else:
		return false;
	endif;
}


 /**
  * buy()
  *
  * @param $user_id
  * @param $ticker
  * @param $num_shares
  * returns human-readable message string
  * I thought about breaking this up, but I like my lengthy buy function
  * because continencies are all in one place
  */
function buy($user_id, $num_shares, $ticker) {
	if (intval($num_shares < 1)):
		return "Must enter an integer number of shares greater than zero. No transaction occured.";
	endif;
	$company_id = get_company_id($ticker);
	if (!$company_id):			//if company not already in db
		$insert_success = insert_company_info($ticker);	//try adding
		if (!$insert_success):
			return "Must enter a valid stock ticker. No transaction occured.";
		endif;
		$company_id = get_company_id($ticker);
	endif;
	$company_id = get_company_id($ticker);			//company in db by now
	$dbh = get_handle();
	$dbh->beginTransaction();						//beginTransaction
	$cash = get_cash($user_id);						//find out how much cash the user has
	//get last trade price
	$quote = fetch_quote($ticker);
	if(!isset($quote['last_trade'])):
		$dbh->rollback();
		return "Couldn't get price quote. Transaction cancelled.";
	endif;
	$price = $quote['last_trade'];
	//multiply last trade by num_shares to determine cost
	$cost = $price * $num_shares;
	//compare cost to cash
	if ($cost > $cash):			//if cost is greater
		$dbh->rollback();		//rollback
		return "Insufficient Funds. Transaction cancelled.";
	endif;
	//if cash is greater or equal
	$to_add = -1 * $cost;
	$added = add_cash($user_id, $to_add);	//remove the cost from the user's cash
	if(!$added):			//if remove cash unsuccessful
		$dbh->rollback();	//rollback
		return "Problem updating your account balance. Transaction cancelled.";
	endif;
	//if remove cash successful
	//from here if there's a problem user must be reimbursed
	$held_already = get_shares_held($user_id, $dbh, $company_id);
	if ($held_already):	//not false or zero, i.e. they do have shares
		$update_success = update_holding($user_id, $company_id, $num_shares+$held_already, $dbh);
		if($update_success):
			$dbh->commit();
			return "Success! You have purchased {$num_shares} shares of {$ticker} for \${$cost}";
		else:
			add_cash($user_id, $cost);
			$dbh->rollback();
		endif;
	else: //if they don't hold that company
		$add_success = add_holding($user_id, $company_id, $num_shares, $dbh);//add holding
		if ($add_success):
			$dbh->commit();						//if add holding successful, commit
			return "Success! You have purchased {$num_shares} shares of {$ticker} for \${$cost}";
		else:
			add_cash($user_id, $cost);
			$dbh->rollback();
		endif;
	endif;
}

/**
 * get_shares_held()
 *
 * prepare a statement which will select the number of shares in a company for a user
 *
 * @param user_id
 * @param database handle $dbh
 * @param company_id
 * @return num_shares
 */
function get_shares_held($user_id, $dbh, $company_id) {
	//prepare query to determine number of shares
	$shares_query = $dbh->prepare("SELECT num_shares FROM holdings
									WHERE user_id=:user_id_bind
									AND company_id=:company_id_bind");
	$shares_query->bindValue(':user_id_bind', $user_id, PDO::PARAM_INT);
	$shares_query->bindValue(':company_id_bind', $company_id, PDO::PARAM_INT);
	$shares_query->execute();
	$shares_result = $shares_query->fetchAll();
	if (!isset($shares_result[0]['num_shares'])) {
		//echo "shares not set";//dbg
		return false;
	}
	if (!$shares_result[0]['num_shares']) {
		//echo "no shares";//dbg
		return false;
	}
	$num_shares = $shares_result[0]['num_shares'];
	return $num_shares;
}

/**
 * sell()
 *
 * Sells ALL of a user's shares in a particular company, calculates their worth.
 * Uses InnoDB transaction to prevent race conditions
 *
 * @param $user_id
 * @param $company_id
 *
 * @return [false | numeric $cash]
 */
function sell($user_id, $company_id) {
	$dbh = get_handle();
	//prepare query to delete holding
	$del_query = $dbh->prepare("DELETE FROM holdings
									WHERE user_id=:user_id_bind
									AND company_id=:company_id_bind");
	$del_query->bindValue(':user_id_bind', $user_id, PDO::PARAM_INT);
	$del_query->bindValue(':company_id_bind', $company_id, PDO::PARAM_INT);
	//prevent race conditions with transaction
	$dbh->beginTransaction();					//beginTransaction
	//determine number of shares held
	$num_shares = get_shares_held($user_id, $dbh, $company_id);
	//get quote to find value of these holdings
	$quote = fetch_quote(get_ticker($company_id));
	if (!isset($quote['last_trade'])) {
		//echo "can't get quote";//dbg
		$dbh->rollback();
		return false;
	}
	$price = $quote['last_trade'];
	$value = $price * $num_shares;
	//execute returns number of rows affected. if zero deleted, but value, problem.
	if (!$del_query->execute() && $value){
		//echo "no row removed";//dbg
		$dbh->rollback();
		return false;
	}
	if (!add_cash($user_id, $value)){
		$dbh->rollback();
		//echo "can't add cash";//dbg
		return false;
	}
	$dbh->commit();
	return $value;
}