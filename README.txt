project1/
	views/
		buy.php
		home.php
		login_successful.php
		login.php
		logout.php
		msg.php				this is loaded by a couple controllers. displays a message passed to it.
		portfolio.php		this also loads the holding view (for each holding)
		holding.php			creates a form offering to sell, showing value of holding
		quote.php			no login required
		register.php
		registration_successful.php
		sell.php
			templates/
				footer.php
				header.php
				nav_bar.php
				title_well.php
	html/
		bootstrap/
		images/
			background.png
		content_loader.php	creates a bootstrap well for some html
		index.php			default controller/dispatcher. loads controllers
	models/
		db_model.php		the entire model
		portfolio_array_struct.txt	not very important. A doc I kept on hand for reference.
	controllers/
		buy.php
		content_loader.php
		home.php
		login.php
		logout.php
		portfolio.php
		quote.php
		register.php
		sell.php
	
	If the user sells stock, they must sell it all. If a user buys stock in a company they already
own, it is updated. No user should have more than one holding entry for one company.
	The database is in third normal form, so companies, users, and holdings all have a table each.
Foreign key constraints are set, with holdings containg child tables. If a company is bought, and
is not in the database, it is added. If at least one other person has bought that company, it should
already be in the database. 